OCP: project with Java exercises for the      
oracle certified programmer exam.     

URL is:     
https://gitlab.com/feifer/ocp

clone command:     
git clone https://gitlab.com/feifer/ocp.git

Don't forget to generate an SSH key pair and add the public key.
for HTTPS communication (!)