package c08_DateTime;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class MyPeriod {

	public MyPeriod() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {
		___separator();
		cout2("current Class: " + MyPeriod.class.getName());

		___separator();
		LocalDate manufactDate = LocalDate.of(2016, Month.JANUARY, 1);
		LocalDate expireDate = LocalDate.of(2018, Month.JULY, 18);
		Period expiry = Period.between(manufactDate, expireDate);
		System.out.printf("Medicine will expire in %d years %d months, and %d days (%s)\n", expiry.getYears(),
				expiry.getMonths(), expiry.getDays(), expiry);
		cout(expiry + ": P = Period in [Y]ears [M]onths [D]ays in one String");

		___separator();
		cout("various Methods:");
		cout("Period.of(5, 4, 3) = " + Period.of(5, 4, 3));
		cout("Period.ofWeeks(6) = " + Period.ofWeeks(6));
		cout("Period.ofMonths(13) = " + Period.ofMonths(13));
		cout("Period.ofDays(23) = " + Period.ofDays(23));
		cout("Period.ofYears(17) = " + Period.ofYears(17));

		___separator();
		cout("Period.parse(\"P4Y6M15D\") = " + Period.parse("P4Y6M15D"));

		___separator();
		cout("How many days until Christmas 2018?");
		LocalDate ldNow = LocalDate.now();
		LocalDate ldXmas18 = LocalDate.of(2018, 12, 24);
		Period pUntilXmas = Period.between(ldNow, ldXmas18);
		cout("Period until Xmas = " + pUntilXmas);
		cout("Days until Xmas = " + pUntilXmas.getDays());
		cout(" The number of days of theat period that is (!)");

	}

}
