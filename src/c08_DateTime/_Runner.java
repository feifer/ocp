package c08_DateTime;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;

public class _Runner {

	public _Runner() {

	}

	public static void main(String[] args) {

		___separator();
		cout2("=== current Class: " + _Runner.class.getName());
		cout("Running all Methods...");
		
		___separator();
		MyDuration.runMethods();
		___separator();
		MyInstant.runMethods();
		___separator();
		MyLocalDate.runMethods();
		___separator();
		MyLocalDateTime.runMethods();
		___separator();
		MyLocalTime.runMethods();
		___separator();
		MyPeriod.runMethods();
	}

}
