package c08_DateTime;

import static util.X.___separator;
import static util.X.cout2;
import static util.X.cout;

import java.time.Clock;
import java.time.LocalTime;
import java.time.ZoneId;

public class MyLocalTime {

	public MyLocalTime() {
		runMethods();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	}

	public static void runMethods() {

		cout2("current Class: " + MyLocalTime.class.getName());

		___separator();
		LocalTime currTime = LocalTime.now();
		cout("LocalTime.now()" + " = " + currTime);

		___separator();
		cout("LocalTime.of(18,30)" + " = " + LocalTime.of(18, 30));

		___separator();
		cout("LocalTime.of(18,30,45)" + " = " + LocalTime.of(18, 30, 45));

		___separator();
		long hours = 6;
		int minutes = 30; // int works as well since int is smaller than long
		cout("current time  is " + " = " + currTime);
		cout("currTime.plusHours(hours).plusMinutes(minutes) = " + currTime.plusHours(hours).plusMinutes(minutes));

		___separator();
		LocalTime ltClock = LocalTime.now(Clock.systemDefaultZone());
		cout("LocalTime.now(Clock.systemDefaultZone() = " + ltClock);

		___separator();
		LocalTime ltZone = LocalTime.now(ZoneId.of("Asia/Tokyo"));
		cout("LocalTime.now(ZoneId.of(\"Asia/Tokyo\") = " + ltZone);

		___separator();
		LocalTime ltSeconds = LocalTime.ofSecondOfDay(7200);
		cout("LocalTime.ofSecondOfDay(7200) = " + ltSeconds);

		___separator();
		LocalTime ltParsed = LocalTime.parse("18:30:05");
		cout("LocalTime.parse(\"18:30:05\") = " + ltParsed);


	}

}
