package c08_DateTime;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class MyQuestions {

	public MyQuestions() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + MyQuestions.class.getName());

		___separator();
		cout("how many days from now to xmas of this (current) year?");
		// get the current Year:
		int currentYear = LocalDate.now().getYear();
		LocalDate ldNow = LocalDate.now();
		LocalDate ldXmas = LocalDate.of(currentYear, 12, 24);
		long lUntilBirthday = ChronoUnit.DAYS.between(ldNow, ldXmas);
		cout("So many days until Xmas: " + lUntilBirthday);

		___separator();
		cout("How many days until my birthday?");

		LocalDate ldMyBDay = LocalDate.of(currentYear, 11, 21);
		long lUntilMyBDay = ChronoUnit.DAYS.between(ldNow, ldMyBDay);
		cout("Days until my BDay " + lUntilMyBDay);

		LocalDate ldSteffieBDay = LocalDate.of(currentYear, 10, 28);
		long lUntilSteffBDay = ChronoUnit.DAYS.between(ldNow, ldSteffieBDay);
		cout("Days until Steffies BDay " + lUntilSteffBDay);

		___separator();
		cout("What weekday was my birthday?");
		LocalDate ldMyBirth = LocalDate.of(1968, 11, 21);
		cout("my Birthday was a " + ldMyBirth.getDayOfWeek());

		cout("Steffie's Birthday was a " + LocalDate.of(1990, 10, 28).getDayOfWeek());
		cout("Justin's Birthday was a " + LocalDate.of(2002, 12, 19).getDayOfWeek());
		cout("Marvin's Birthday was a " + LocalDate.of(2004, 10, 26).getDayOfWeek());
		cout("Albert's Birthday was a " + LocalDate.of(1928, 3, 20).getDayOfWeek());

		___separator();
		cout("How many days is Justin older than Marvin?");
		cout("Justins Birthday: 19.12.2002");
		cout("Marvins Birthday: 26.10.2004");

		LocalDate ldtJustin = LocalDate.of(2002, 12, 19);
		LocalDate ldtMarvin = LocalDate.of(2004, 10, 26);
		Period pDiff = Period.between(ldtJustin, ldtMarvin);
		cout("Days between Justin and Marvin: " + pDiff);
		cout("Days between Justin and Marvin: " + pDiff.getDays());
		cout("Days between Justin and Marvin: " + pDiff.get(ChronoUnit.DAYS));

		long lDiffDays = ChronoUnit.DAYS.between(ldtJustin, ldtMarvin);
		cout("Days between Justin and Marvin: " + lDiffDays);

		cout("Days between Justin and Marvin: " + ldtMarvin.until(ldtJustin));
		cout("Days between Justin and Marvin: " + ldtMarvin.until(ldtJustin, ChronoUnit.DAYS));

		___separator();
		cout("How old would Einstein become this (current) year?");
		LocalDate ldEinstein = LocalDate.of(1879, 3, 14);
		cout("Einsteins would be this year: " + ldEinstein.until(LocalDate.now(), ChronoUnit.YEARS));
	}
}
