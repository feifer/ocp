/**
 * 
 */
package c08_DateTime;

import static util.X.cout;
import static util.X.cout2;
import static util.X.___separator;

import java.time.Clock;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

/**
 * @author Fifi
 *
 */
public class MyLocalDate {

	/**
	 * 
	 */
	public MyLocalDate() {

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		___separator();
		cout2("current Class: " + MyLocalDate.class.getName());

		___separator();
		LocalDate today = LocalDate.now();
		cout("CODE: LocalDate today = LocalDate.now();");
		cout("   => today's date is: " + today);

		___separator();
		LocalDate newYear2016 = LocalDate.of(2016, 1, 1);
		cout("New Year 2016: " + newYear2016);

		___separator();
		cout("CODE: LocalDate valentinesDay = LocalDate.of(2016,  14,  2);");
		cout("will produce EXCEPTION: Invalid value for MonthOfYear (valid values 1 - 12): 14");
		// LocalDate valentinesDay = LocalDate.of(2016, 14, 2);
		// cout("Valentine's day: " + valentinesDay);

		___separator();
		LocalDate valentinesDay = LocalDate.of(2016, Month.FEBRUARY, 2);
		cout("CODE: LocalDate valentinesDay = LocalDate.of(2016, Month.FEBRUARY, 2);");
		cout("Valentine's day: " + valentinesDay);

		___separator();
		long visaValidatyDays = 180L;
		LocalDate currDate = LocalDate.now();
		cout("My Visa expires on: " + currDate.plusDays(visaValidatyDays));

		___separator();
		LocalDate nowClock = LocalDate.now(Clock.systemDefaultZone());
		cout("LocalDate.now(Clock.systemDefaultZone()) = " + nowClock);

		___separator();
		LocalDate nowZoneIDKolkata = LocalDate.now(ZoneId.of("Asia/Kolkata"));
		cout("LocalDate.now(ZoneId.of(\"Asia/Kolkata\") = " + nowZoneIDKolkata);

		___separator();
		LocalDate nowZoneID = LocalDate.now(ZoneId.of("Asia/Tokyo"));
		cout("LocalDate.now(ZoneId.of(\"Asia/Tokyo\") = " + nowZoneID);

		___separator();
		LocalDate ldOfYearDay = LocalDate.ofYearDay(2016, 100);
		cout("LocalDate.ofYearDay(2016, 100) = " + ldOfYearDay);

		___separator();
		LocalDate ldParsed = LocalDate.parse("1968-11-21");
		cout("LocalDate.parse(\"1968-11-21\") = " + ldParsed);

		___separator();
		LocalDate ldEpochDay = LocalDate.ofEpochDay(50);
		cout("LocalDate.ofEpochDay(50) = " + ldEpochDay);

	}

}
