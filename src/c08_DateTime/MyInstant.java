package c08_DateTime;

import static util.X.___separator;
import static util.X.cout2;
import static util.X.cout;

import java.time.Instant;

public class MyInstant {

	public MyInstant() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		___separator();
		cout2("current Class: " + MyInstant.class.getName());


		___separator();
		Instant currTimeStamp = Instant.now();
		cout(" Instant.now() = " + currTimeStamp);
		

		___separator();
		cout("Number of seconds elapsed since unix Epoch start: ");
		cout(" currTimeStamp.getEpochSecond() = " + currTimeStamp.getEpochSecond());
		cout(" currTimeStamp.toEpochMilli() =   " + currTimeStamp.toEpochMilli());

		___separator();
		cout("Note that LocalDateTime and Instant differ if LocalDateTime bases on time zones different from Greenwich.");

	}

}
