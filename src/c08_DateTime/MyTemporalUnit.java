package c08_DateTime;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class MyTemporalUnit {

	public MyTemporalUnit() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + MyTemporalUnit.class.getName());
		
		___separator();
		cout("Duration.of(1, ChronoUnit.MINUTES).getSeconds() = " + Duration.of(1, ChronoUnit.MINUTES).getSeconds());
		cout("Duration.of(1, ChronoUnit.HOURS).getSeconds() = " + Duration.of(1, ChronoUnit.HOURS).getSeconds());
		cout("Duration.of(1, ChronoUnit.DAYS).getSeconds() = " + Duration.of(1, ChronoUnit.DAYS).getSeconds());

	}
}
