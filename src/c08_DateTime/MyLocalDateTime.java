package c08_DateTime;

import static util.X.___separator;
import static util.X.cout;

import java.time.LocalDateTime;
import static util.X.cout2;

public class MyLocalDateTime {

	public MyLocalDateTime() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		___separator();
		cout2("current Class: " + MyLocalDateTime.class.getName());

		___separator();
		LocalDateTime currDateTime = LocalDateTime.now();
		cout("LocalDateTime.now()" + " = " + currDateTime);

		___separator();
		LocalDateTime ldtXmas = LocalDateTime.of(2015, 12, 25, 0, 0);
		LocalDateTime ldtNewYear = LocalDateTime.of(2016, 1, 1, 0, 0);
		cout("is New Year 2016 after Xmas 2015? " + ldtNewYear.isAfter(ldtXmas));

		___separator();
		cout("LocalDateTime.now()" + " = " + currDateTime);
		cout("Date Component: currDateTime.toLocalDate()" + " = " + currDateTime.toLocalDate());
		cout("Time Component: currDateTime.toLocalTime()" + " = " + currDateTime.toLocalTime());

	}

}
