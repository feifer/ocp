package c08_DateTime;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;

import java.time.ZoneId;
import java.util.Set;

public class MyZoneIdsAll {

	public MyZoneIdsAll() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + MyZoneIdsAll.class.getName());
		
		___separator();
		cout("MY ZoneId is: " + ZoneId.systemDefault());
		
		___separator();
		cout("Now Listing ALL Available ZoneIds: ");
		
		
		Set<String> zones = ZoneId.getAvailableZoneIds();
		cout ("Number of available time zones: " + zones.size());
		zones.forEach(System.out::println);
		
	}

}
