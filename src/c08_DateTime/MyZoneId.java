package c08_DateTime;

import static util.X.___separator;
import static util.X.cout2;
import static util.X.cout;

import java.time.ZoneId;

public class MyZoneId {

	public MyZoneId() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + MyZoneId.class.getName());
		
		___separator();
		cout("MY ZoneId is: " + ZoneId.systemDefault());
		
		___separator();
		
		
	}
}
