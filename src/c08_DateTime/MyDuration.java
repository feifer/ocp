package c08_DateTime;

import static util.X.___separator;
import static util.X.cout2;
import static util.X.cout;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class MyDuration {

	public MyDuration() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + MyDuration.class.getName());

		cout("Duration is the counterpart of Period!");
		cout("PEriod workds with DAYS. Duration works with hours");

		___separator();
		cout("Duration.of(3600,  ChronoUnit.MINUTES) = " + Duration.of(3600, ChronoUnit.MINUTES));
		cout(" Duration.ofDays(4) = " + Duration.ofDays(4));
		cout(" Duration.ofDays(14) = " + Duration.ofDays(14));
		cout(" Duration.ofHours(2) = " + Duration.ofHours(2));
		cout(" Duration.ofMinutes(15) = " + Duration.ofMinutes(15));
		cout(" Duration.ofSeconds(30) = " + Duration.ofSeconds(30));
		cout(" Duration.ofMillis(120) = " + Duration.ofMillis(120));
		cout(" Duration.ofNanos(120) = " + Duration.ofNanos(120));
		cout("" + Duration.parse("P2DT10H30M"));
		cout("");

	}

}
