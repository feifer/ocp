package c06_Streams;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Solution {

	static int sockMerchant3(int n, int[] ar) {

		int sum = Arrays.stream(ar).boxed()
				.collect(Collectors.groupingBy(Integer::intValue)).values()
				.stream().mapToInt(i -> i.size() / 2).sum();

		return sum;
	}

	// Complete the sockMerchant function below.
	static int sockMerchant2(int n, int[] ar) {

		AtomicInteger ordinal = new AtomicInteger(0);
		Arrays.stream(ar).boxed()
				.collect(Collectors.groupingBy(Integer::intValue))
				.forEach((colour, count) -> {
					ordinal.getAndSet(ordinal.get() + count.size() / 2);
				});

		return ordinal.get();
	}

	// Complete the sockMerchant function below.
	static int sockMerchant(int n, int[] ar) {

		// IntStream.range(1, 10).forEach(System.out::println);

		int pairs = 0;
		int matches = 0;
		for (int jj = 1; jj <= 100; jj++) {
			matches = 0;
			for (int ii : ar) {

				// System.out.println(ii);
				if (jj == ii) {
					matches++;
				}
			}
			// System.out.println(jj + ": " + pairs + " " + matches);
			if (matches % 2 == 0) {
				pairs += matches / 2;
			} else {
				pairs += (matches - 1) / 2;
			}
		}
		// System.out.println(": " + pairs + " " + matches);

		return pairs;
	}

	// private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		// BufferedWriter bufferedWriter = new BufferedWriter(
		// new FileWriter(System.getenv("OUTPUT_PATH")));
		//
		// int n = scanner.nextInt();
		// scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
		//
		// int[] ar = new int[n];
		//
		// String[] arItems = scanner.nextLine().split(" ");
		// scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
		//
		// for (int i = 0; i < n; i++) {
		// int arItem = Integer.parseInt(arItems[i]);
		// ar[i] = arItem;
		// }

		// int[] ar = new int[] { 10, 20, 20, 10, 10, 30, 50, 10, 20 };
		int[] ar = new int[] { 1, 1, 100, 100 };
		int n = 9;

		int result = sockMerchant3(n, ar);
		System.out.println("result =" + result);

		// bufferedWriter.write(String.valueOf(result));
		// bufferedWriter.newLine();
		//
		// bufferedWriter.close();
		//
		// scanner.close();
	}
}
