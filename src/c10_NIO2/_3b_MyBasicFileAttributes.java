package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;

public class _3b_MyBasicFileAttributes {

	public _3b_MyBasicFileAttributes() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _3b_MyBasicFileAttributes.class.getName());
		cout("Testing BasicFileAttributes");

		___separator();
		cout("-1-");
		Path myFile = Paths.get("testFile.txt");
		cout("attribute data about " + myFile.toAbsolutePath());
		cout("Version THREE to do it...");

		try {
			BasicFileAttributes fileAttributes = Files.readAttributes(myFile, BasicFileAttributes.class);
			coutln("File size: " + fileAttributes.size());
			cout("isDirectory: " + fileAttributes.isDirectory());
			cout("isRegularFile: " + fileAttributes.isRegularFile());
			cout("isSymbolicLink: " + fileAttributes.isSymbolicLink());
			cout("File last accessed time: " + fileAttributes.lastAccessTime());
			cout("File last modified time: " + fileAttributes.lastModifiedTime());
			cout("File creation time: " + fileAttributes.creationTime());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}// </runMethods>

}// </class>
