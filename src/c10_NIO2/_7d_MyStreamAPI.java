package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class _7d_MyStreamAPI {

	public _7d_MyStreamAPI() {

	}

	public static void main(String[] args) throws IOException {
		runMethods();
	}

	public static void runMethods() throws IOException {

		cout2("current Class: " + _7d_MyStreamAPI.class.getName());

		cout("Testing Streams and NIO.2");

		___separator();
		coutln("SYNTAX:");
		cout("Stream<String> lines = Files.lines(Paths.get(\"testFile.txt\")) \n");

		cout("FILE CONTENT:");
		cout("\"");
		
		try (Stream<String> lines = Files.lines(Paths.get("testFile.txt"))) {
			lines.forEach(System.out::println);
		} catch (IOException ioe) {
			System.err.println("IOException occurred when reading the file... exiting");
			System.exit(-1);
		}
		cout("\"");
	}
}
