package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class _5_MyMove {

	public _5_MyMove() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _5_MyMove.class.getName());
		cout("Testing Files.move Method");

		// ___separator();
		// coutln("eventual CLEANUP:");
		// try {
		// Files.deleteIfExists(Paths.get("TEST6\\renamed.txt"));
		// // Files.deleteIfExists(Paths.get("TEST6"));
		// } catch (IOException e1) {
		// e1.printStackTrace();
		// }

		___separator();
		cout("-1-");
		coutln("CASE: move a file somewhere else");
		Path file2Move = Paths.get("moveme.txt");
		Path movedFile = Paths.get("TEST6\\moveme.txt");
		Path targetDir = Paths.get("TEST6");
		Path renamedFile = Paths.get("TEST6\\renamed.txt");
		Path targetDir2 = Paths.get("TEST7");
		Path targetDir3 = Paths.get("TEST8");

		try {
			coutln("-2-");
			cout("file2Move = " + file2Move);
			cout("RULE: if source file does not exist -> java.nio.file.NoSuchFileException");
			if (!Files.exists(file2Move, LinkOption.NOFOLLOW_LINKS)) {
				Files.createFile(file2Move);
			}

			coutln("-3-");
			cout("targetDir = " + targetDir);
			cout("RULE: if intermediate directory does not exist -> java.nio.file.NoSuchFileException");
			if (!Files.exists(targetDir, LinkOption.NOFOLLOW_LINKS)) {
				Files.createDirectory(targetDir);
			}

			coutln("-4-");
			cout("RULE: if target file already exists -> java.nio.file.FileAlreadyExistsException");
			// Files.move(file2Move, movedFile);
			cout("file2Move = " + file2Move);
			cout("movedFile = " + movedFile);
			Files.move(file2Move, movedFile,
					StandardCopyOption.REPLACE_EXISTING);

			___separator();
			coutln("-5-");
			cout("movedFile = " + movedFile);
			cout("CASE: move a file on to itself  -> will it work?");
			Files.move(movedFile, movedFile,
					StandardCopyOption.REPLACE_EXISTING);

			coutln("CONCLUSION: moving a file onto itself works but the move command 'has no effect'.)");

			___separator();
			cout("-6-");
			cout("movedFile = " + movedFile);
			cout("renamedFile = " + renamedFile);
			Files.move(movedFile, renamedFile,
					StandardCopyOption.REPLACE_EXISTING);
			coutln("CONCLUSION: moving a file inside the same directory is actually RENAMING the file)");

			___separator();
			cout("-7-");
			coutln("CASE: move a directory -> will it work?");
			if (!Files.exists(targetDir2, LinkOption.NOFOLLOW_LINKS)) {
				Files.createDirectory(targetDir2);
			}

			cout("targetDir2 = " + targetDir2);
			cout("targetDir3 = " + targetDir3);
			Files.move(targetDir2, targetDir3,
					StandardCopyOption.REPLACE_EXISTING);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
