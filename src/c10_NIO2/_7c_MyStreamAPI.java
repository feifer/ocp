package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.BiPredicate;
import java.util.stream.Stream;

public class _7c_MyStreamAPI {

	public _7c_MyStreamAPI() {
	}

	public static void main(String[] args) throws IOException {
		runMethods();
	}

	public static void runMethods() throws IOException {

		cout2("current Class: " + _7c_MyStreamAPI.class.getName());

		cout("Testing Streams and NIO.2");

		coutln("NOTE: the 'throws IOException'");

		___separator();
		coutln("SYNTAX:");
		cout("Stream<Path> entries = Files.find(Paths.get(\".\"), 4, predicate)) ");

		BiPredicate<Path, BasicFileAttributes> predicate = (path, attrs) -> attrs.isRegularFile()
				&& path.toString().endsWith("class");
		// check this variation ending: path.endsWith("class");

		try (Stream<Path> entries = Files.find(Paths.get("."), 4, predicate)) {
			entries.limit(100).forEach(System.out::println);
		}
	}

}
