package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class _2_MyPathCompare {

	public _2_MyPathCompare() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _2_MyPathCompare.class.getName());

		___separator();
		cout("-1-");
		Path indirectPath = Paths.get("Test");
		Path absolutePath = Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\Test");

		cout("Paths.get(\"Test\")");
		cout("Paths.get(\"c:\\DEV\\eclipse_neo_workspace\\ocp\\Test\")");
		
		// retrieve information:
		cout("indirectPath.getFileName() = " + indirectPath.getFileName());
		cout("absolutePath.getFileName() = " + absolutePath.getFileName());

		cout("(indirectPath.compareTo(absolutePath) == 0) " + (indirectPath.compareTo(absolutePath) == 0));

		cout("indirectPath.equals(absolutePath) " + indirectPath.equals(absolutePath));

		// ---
		coutln("-2-");
		Path indirect2Absolute = indirectPath.toAbsolutePath();
		Path absolute2Absolute = absolutePath.toAbsolutePath();

		cout("indirect2Absolute  " + indirect2Absolute);
		cout("absolute2Absolute  " + absolute2Absolute);

		cout("(indirect2Absolute.compareTo(absolute2Absolute) == 0) "
				+ (indirect2Absolute.compareTo(absolute2Absolute) == 0));
		cout("indirect2Absolute.equals(absolute2Absolute) " + indirect2Absolute.equals(absolute2Absolute));

		___separator();
		cout("-3-");
		cout("BUT WAIT: there's more!");
		cout("Use Files.isSameFile() for better results:");

		try {
			cout("Files.isSameFile(indirectPath, absolutePath) = " + Files.isSameFile(indirectPath, absolutePath));
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
