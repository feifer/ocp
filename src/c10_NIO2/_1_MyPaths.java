package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class _1_MyPaths {

	public _1_MyPaths() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _1_MyPaths.class.getName());
		cout("--> Testing Paths class");

		___separator();
		cout("-1-");
		Path testFilePath = Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\testFile.txt");

		// TRY THIS:
		// Path testFilePath =
		// Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\..");

		// TRY THIS:
		// Path testFilePath =
		// Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\..\\testFile.txt");

		// retrieve information:
		cout("testFilePath = " + testFilePath);
		cout("testFilePath.getFileName() = " + testFilePath.getFileName());
		cout("testFilePath.getRoot()   = " + testFilePath.getRoot());

		coutln("SPECIAL: what will the following print?");
		cout("testFilePath.getRoot().getFileName()   = " + testFilePath.getRoot().getFileName());
		cout("Files.exists(testFilePath.getRoot()) = " + Files.exists(testFilePath.getRoot()));
		cout("Files.isDirectory(testFilePath.getRoot()) = " + Files.isDirectory(testFilePath.getRoot()));

		coutln("testFilePath.getParent() = " + testFilePath.getParent());
		cout("testFilePath.normalize() = " + testFilePath.normalize());
		cout("testFilePath.isAbsolute() = " + testFilePath.isAbsolute());

		___separator();
		cout("-2-");
		cout("printing out elements of the path:");
		for (Path element : testFilePath) {
			cout("\t path element: " + element);
		}

		coutln("NOTE: c:\\ is ROOT -> not part of the path");

		___separator();
		cout("-3-");
		cout("testFilePath = " + testFilePath);
		cout("testFilePath.getNameCount() = " + testFilePath.getNameCount());
		cout("testFilePath.getName(0) = " + testFilePath.getName(0));
		cout("testFilePath.getName(3) = " + testFilePath.getName(3));
		// causes java.lang.IllegalArgumentException
		// cout("testFilePath.getName(4) = " + testFilePath.getName(4));

		___separator();
		cout("-4-");
		Path test2FilePath = Paths.get(".\\Test");
		cout("Paths.get(\".\\Test\")");
		cout("test2FilePath.getFileName() = " + test2FilePath.getFileName());
		cout("test2FilePath.toUri() = " + test2FilePath.toUri());
		cout("test2FilePath.toAbsolutePath() = " + test2FilePath.toAbsolutePath());
		cout("test2FilePath.normalize() = " + test2FilePath.normalize());
		cout("test2FilePath.isAbsolute() = " + test2FilePath.isAbsolute());

		try {
			cout("test2FilePath.toRealPath(LinkOption.NOFOLLOW_LINKS) = "
					+ test2FilePath.toRealPath(LinkOption.NOFOLLOW_LINKS));
			cout("RULE: If it does not exist you get:  \"java.nio.file.NoSuchFileException\"");
		} catch (IOException e) {
			e.printStackTrace();
		}

		___separator();
		cout("-5-");
		Path dirName = Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\");
		Path resolvedPath = dirName.resolve("Test");
		cout("dirName.resolve(\"Test\") = " + resolvedPath);
		cout("RULE: resolve() JOINS two paths.");
		cout("RULE: throws InvalidPathException - if the path string cannot be converted to a Path");

		___separator();
		cout("-6-");
		Path myPath = Paths.get("/non/exisitng");
		Path joined = myPath.resolve("at/all");
		cout("joined path = " + joined);
		cout("-> watch how path separators are changed according to OS...");

		___separator();
		cout("-7-");
		Path mySubpath = Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\Test");
		cout("mySubpath.subpath(0, 3) = " + mySubpath.subpath(0, 3));

		Path mySubpath2 = Paths.get("Test");
		// throws: java.lang.IllegalArgumentException
		// cout("mySubpath2.subpath(0, 3)" + mySubpath2.subpath(0, 3));

		coutln("BUT:");

		___separator();
		cout("-8-");
		cout("mySubpath2.toAbsolutePath().subpath(0, 3) = " + mySubpath2.toAbsolutePath().subpath(0, 3));
		Path myStartPath = Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\Test");
		cout("myStartPath = " + myStartPath);
		cout("myStartPath.startsWith(mySubpath2) = " + myStartPath.startsWith(mySubpath2));
		cout("myStartPath.startsWith(\"c:\") = " + myStartPath.startsWith("c:"));
		cout("myStartPath.startsWith(\"c:\\\") = " + myStartPath.startsWith("c:\\"));
		cout("-> Errata on the book: Return type is boolean! -> p288");
	}
}
