package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class _7b_MyStreamAPI {

	public _7b_MyStreamAPI() {
	}

	public static void main(String[] args) throws IOException {
		runMethods();
	}

	public static void runMethods() throws IOException {

		cout2("current Class: " + _7b_MyStreamAPI.class.getName());

		cout("Testing Streams and NIO.2");
		coutln("NOTE: the 'throws IOException'");

		___separator();
		coutln("SYNTAX:");
		cout("tream<Path> entries = Files.walk(Paths.get(\".\"), 4, FileVisitOption.FOLLOW_LINKS) \n");

		try (Stream<Path> entries = Files.walk(Paths.get("."), 4, FileVisitOption.FOLLOW_LINKS)) {
			
			long numOfEntries = entries.count();
			System.out.printf("Found %d entries in the current path", numOfEntries);
		}
	}

}
