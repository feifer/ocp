package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class _4_MyCopy {

	public _4_MyCopy() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _4_MyCopy.class.getName());

		cout("Testing Files.copy Method");

		___separator();
		cout("-1-");
		coutln("standard case: copy file from source to destination: -> what happens?");
		cout("TODO");
		Path myFile = Paths.get("testFile3.txt");
		Path myTarget = Paths.get("TEST2");
		try {
			Files.copy(myFile, myTarget, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
		cout("SPECIAL: directory will turn into file (!)");
		cout("SPECIAL: the file will not land inside the directory");
		cout("SPECIAL: removing the StandardCopyOption will not help here.");

		// copy over existing
		___separator();
		cout("-2-");
		coutln("case: copy file over existing file: (same, same but different :-)");
		try {
			cout("Files.copy(myFile, myTarget, StandardCopyOption.REPLACE_EXISTING)"
					+ Files.copy(myFile, myTarget, StandardCopyOption.REPLACE_EXISTING));
		} catch (IOException e) {
			e.printStackTrace();
		}

		// copy to same file
		___separator();
		cout("-3-");
		coutln("case: copy file over itself: will it work?");
		try {
			cout("Files.copy(myFile, myFile, StandardCopyOption.REPLACE_EXISTING)"
					+ Files.copy(myFile, myFile, StandardCopyOption.REPLACE_EXISTING));
		} catch (IOException e) {
			e.printStackTrace();
		}
		cout("SPECIAL: it will work but the copy will not be made (!)");

		// copy file to new file:
		___separator();
		cout("-4-");
		coutln("CASE: copy file to a new file");
		Path newFile = Paths.get("testFile4.txt");
		try {
			Files.copy(myFile, newFile, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// copy directory with content
		___separator();
		cout("-5-");
		coutln("CASE: copy directory with content:");
		Path newDir1 = Paths.get("TEST3");
		Path newDir2 = Paths.get("TEST5");
		try {
			// Files.copy(newFile, newDir1,
			// StandardCopyOption.REPLACE_EXISTING);
			Files.copy(newDir1, newDir2, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// copy file to non existing dir -> same as copy file to new file

		// try out: copy directory to file file will turn into directory (if not
		// empty)

		// if you copy files to a path: make sure the full path exists!
		// it will not get created!
	}
}
