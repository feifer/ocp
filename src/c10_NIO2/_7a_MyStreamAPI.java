package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class _7a_MyStreamAPI {

	public _7a_MyStreamAPI() {

	}

	public static void main(String[] args) throws IOException {
		runMethods();
	}

	public static void runMethods() throws IOException {

		cout2("current Class: " + _7a_MyStreamAPI.class.getName());

		cout("Testing Streams and NIO.2");
		
		coutln("RULE: 'throws java.io.IOException'");

		___separator();
		cout("Now using: Files.list(...)");
		coutln("SYNTAX:");
		cout("Stream<Path> entries = Files.list(Paths.get(\".\"))\n");
		try (Stream<Path> entries = Files.list(Paths.get("."))) {
			
			entries.forEach(System.out::println);
			
			//  TRY THIS:
			// entries.map(path -> path.toAbsolutePath()).forEach(System.out::println);
			
			// NOW TRY THIS!
			//entries.map(path -> path.toAbsolutePath().normalize()).forEach(System.out::println);
			
		}
	}

}
