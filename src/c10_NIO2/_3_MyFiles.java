package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
// import java.nio.file.attribute.UserPrincipal;

public class _3_MyFiles {

	public _3_MyFiles() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _3_MyFiles.class.getName());
		cout("Testing Files class");

		___separator();
		cout("-1-");
		coutln("DOES NOT COMPILE:");
		cout("Files.createDirectory(\"Test2\") -> WHY?");

		___separator();
		cout("-2-");
		try {
			Path createTest2 = Paths.get("TEST2");
			cout("Path createTest2 = Paths.get(\"TEST2\");");
			Path createdTest2 = Files.createDirectory(createTest2);
			cout("createdTest2 = " + createdTest2);
			cout("createdTest2.toAbsolutePath() = " + createdTest2.toAbsolutePath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		___separator();
		cout("-3-");
		try {
			Path createTest34 = Paths.get("TEST3\\TEST4");
			cout("Path createTest34 = Paths.get(\"TEST3\\TEST4\");");
			Path createdTest34 = Files.createDirectories(createTest34);
			cout("createdTest34 = " + createdTest34);
			cout("createdTest34.toAbsolutePath() = " + createdTest34.toAbsolutePath());
			cout("Files.isDirectory(createdTest34, LinkOption.NOFOLLOW_LINKS) = "
					+ Files.isDirectory(createdTest34, LinkOption.NOFOLLOW_LINKS));
		} catch (IOException e) {
			e.printStackTrace();
		}

		___separator();
		cout("-4-");
		cout("what happens here? check the created path!");
		Path testFilePath = Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\TEST3\\TEST4\\testFile.txt");

		// retrieve information:
		cout("testFilePath = " + testFilePath);
		try {
			Path tfp = Files.createDirectory(testFilePath);
			cout("tfp = " + tfp);
			cout("tfp.toAbsolutePath() = " + tfp.toAbsolutePath());
			cout("Files.isDirectory(tfp, LinkOption.NOFOLLOW_LINKS) = "
					+ Files.isDirectory(tfp, LinkOption.NOFOLLOW_LINKS));
		} catch (IOException e) {
			e.printStackTrace();
		}

		___separator();
		cout("-5-");
		cout("-> SPECIAL. root path");
		Path myRoot = Paths.get("c:\\");
		cout("myRoot = " + myRoot);
		cout("Files.exists(myRoot, LinkOption.NOFOLLOW_LINKS) = " + Files.exists(myRoot, LinkOption.NOFOLLOW_LINKS));
		cout("Files.isDirectory(myRoot, LinkOption.NOFOLLOW_LINKS) = "
				+ Files.isDirectory(myRoot, LinkOption.NOFOLLOW_LINKS));

		___separator();
		cout("-6-");
		Path myFile = Paths.get("DUMMY.txt"); // shut up "may not have been initialized"...
		try {
			myFile = Files.createFile(Paths.get("testFile3.txt"));
			
			cout("Files.createFile(Paths.get(\"testFile3.txt\")");
			cout("RULE: throws java.nio.file.FileAlreadyExistsException -if file does already exist.");
		} catch (IOException e) {
			e.printStackTrace();
		}

		coutln("myFile = " + myFile);
		cout("myFile.toAbsolutePath() = " + myFile.toAbsolutePath());

		coutln("Reading information on the file. Version ONE to do it: ");

		coutln("-> CHECK 1: isdirectory?");
		cout("Files.isDirectory(myFile, LinkOption.NOFOLLOW_LINKS) = "
				+ Files.isDirectory(myFile, LinkOption.NOFOLLOW_LINKS));

		coutln("-> CHECK 2: isRegularFile?");
		Boolean b = false;
		cout("Files.isRegularFile(myFile, LinkOption.NOFOLLOW_LINKS) = "
				+ (b = Files.isRegularFile(myFile, LinkOption.NOFOLLOW_LINKS)));

		coutln("-> CHECK 3: isExecutable?");
		cout("Files.isExecutable(myFile) = " + Files.isExecutable(myFile));

		coutln("-> CHECK 4: isSymbolicLink?");
		cout("Files.isSymbolicLink(myFile) = " + Files.isSymbolicLink(myFile));

		coutln("-> CHECK 5: exists?");
		cout("Files.Files.exists(myFile, LinkOption.NOFOLLOW_LINKS) = "
				+ Files.exists(myFile, LinkOption.NOFOLLOW_LINKS));

		coutln("-> CHECK 6: notExists?");
		cout("Files.Files.notExists(myFile, LinkOption.NOFOLLOW_LINKS) = "
				+ Files.notExists(myFile, LinkOption.NOFOLLOW_LINKS));

		try {
			coutln("-> CHECK 7: isHidden?");
			cout("Files.Files.isHidden(myFile) = " + Files.isHidden(myFile));

			coutln("-> CHECK 8: size?");
			long lSize;
			cout("Files.Files.size(myFile) = " + (lSize = Files.size(myFile)));

			coutln("-> CHECK 9: WHO owns it?");
			cout("Files.getOwner(myFile, LinkOption.NOFOLLOW_LINKS) = "
					+ Files.getOwner(myFile, LinkOption.NOFOLLOW_LINKS));

			coutln("-> HACK: change owner!");
			cout("ehm - postponed :-)");
			// UserPrincipal fileOwner = Files.getOwner(myFile);
			// cout("Files.setOwner(myFile, LinkOption.NOFOLLOW_LINKS) = "
			// + Files.setOwner(myFile, fileOwner));

			coutln("-> CHECK 10: when last modified?");
			cout("Files.getLastModifiedTime(myFile, LinkOption.NOFOLLOW_LINKS) = "
					+ Files.getLastModifiedTime(myFile, LinkOption.NOFOLLOW_LINKS));

			coutln("-> HACK: setLastModifiedTime!");
			FileTime now = FileTime.fromMillis(System.currentTimeMillis());
			cout("Files.setLastModifiedTime(myFile, now ) = " + Files.setLastModifiedTime(myFile, now));

			cout("Files.getLastModifiedTime(myFile, LinkOption.NOFOLLOW_LINKS) = "
					+ Files.getLastModifiedTime(myFile, LinkOption.NOFOLLOW_LINKS));

		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
