package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class _3a_MyFileAttributes {

	public _3a_MyFileAttributes() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _3a_MyFileAttributes.class.getName());
		cout("Testing FileAttributes");

		coutln("LinkOption nfl = LinkOption.NOFOLLOW_LINKS;");
		cout("-> would be a temptation BUT: for better learning ");
		cout("we stick to original syntax!");

		___separator();
		cout("-1-");
		Path myFile = Paths.get("testFile.txt");

		cout("attribute data about " + myFile.toAbsolutePath());
		cout("Version TWO to do it...");
		try {
			Object object = Files.getAttribute(myFile, "creationTime", LinkOption.NOFOLLOW_LINKS);
			coutln("Creation time: " + object);
			object = Files.getAttribute(myFile, "lastModifiedTime", LinkOption.NOFOLLOW_LINKS);
			cout("Last modified time: " + object);
			object = Files.getAttribute(myFile, "size", LinkOption.NOFOLLOW_LINKS);
			cout("Size: " + object);
			object = Files.getAttribute(myFile, "dos:hidden", LinkOption.NOFOLLOW_LINKS);
			cout("isHidden: " + object);
			object = Files.getAttribute(myFile, "isDirectory", LinkOption.NOFOLLOW_LINKS);
			cout("isDirectory: " + object);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
