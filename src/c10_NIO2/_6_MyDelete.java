package c10_NIO2;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

public class _6_MyDelete {

	public _6_MyDelete() {

	}

	public static void main(String[] args) {
		runMethods();
	}

	public static void runMethods() {

		cout2("current Class: " + _6_MyDelete.class.getName());
		coutln("Testing Files.delete Method");

		___separator();
		cout("-1-");
		Path testFilePath = Paths.get("c:\\DEV\\eclipse_neo_workspace\\ocp\\TEST3\\TEST4\\testFile.txt");

		if (Files.exists(testFilePath, LinkOption.NOFOLLOW_LINKS)) {
			cout("File exists -> so we delete it: " + testFilePath);
			try {
				cout(" Files.delete(" + testFilePath + ")");
				Files.delete(testFilePath);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			cout("File does not exist: " + testFilePath);
			cout("-> no need to DELETE :)");
		}

		___separator();
		cout("-2-");
		coutln("how about a little convenience? ;-)");
		cout("-> Files.deleteIfExists(testFilePath) to the rescue!");

		try {
			coutln("delete was a success?");
			cout(" Files.deleteIfExists(" + testFilePath + ") = " + Files.deleteIfExists(testFilePath));
		} catch (IOException e) {
			e.printStackTrace();
		}

		___separator();
		cout("-3-");
		// ACTION: INSERT SNIPPET HERE
		
		// ---
		// ACTION: shift this action before the other delete actions! will it work?
		___separator();
		cout("-5-");
		Path createTest3 = Paths.get("TEST3");
		try {
			cout(" Files.deleteIfExists(" + createTest3 + ") = " + Files.deleteIfExists(createTest3));
		} catch (IOException e) {
			e.printStackTrace();
		}
		// ---


		___separator();
		cout("-4-");
		coutln("now deleteIfExists all other test directories and files");
		Path copyFile = Paths.get("TEST2\\testFile3.txt");

		Path createTest2 = Paths.get("TEST2");
		Path createTest34 = Paths.get("TEST3\\TEST4");
		Path myFile = Paths.get("testFile3.txt");
		Path myFile2 = Paths.get("testFile4.txt");

		try {
			cout(" Files.deleteIfExists(" + copyFile + ") = " + Files.deleteIfExists(copyFile));
			cout(" Files.deleteIfExists(" + myFile2 + ") = " + Files.deleteIfExists(myFile2));

			cout(" Files.deleteIfExists(" + createTest2 + ") = " + Files.deleteIfExists(createTest2));
			cout(" Files.deleteIfExists(" + createTest34 + ") = " + Files.deleteIfExists(createTest34));
			cout(" Files.deleteIfExists(" + myFile + ") = " + Files.deleteIfExists(myFile));

			// other files:
			cout(" Files.deleteIfExists(" + myFile + ") = " + Files.deleteIfExists(myFile));
		} catch (IOException e) {
			e.printStackTrace();
		}


	}// </runMethods>

}// </MyDelete>
