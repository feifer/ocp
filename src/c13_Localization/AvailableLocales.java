package c13_Localization;

import java.util.Arrays;
import java.util.Locale;
//import static util.X.___separator;
import static util.X.cout;
//import static util.X.cout2;
import static util.X.coutln;

public class AvailableLocales {

	public AvailableLocales() {

	}

	public static void main(String[] args) {

		
		cout("The default locale is: " + Locale.getDefault());

		coutln("get all available locales...");

		Locale[] locales = Locale.getAvailableLocales();

		System.out.printf(
				"TOTAL Number of available locales: >>%d<<. %n",
				locales.length);

		Arrays.stream(locales).forEach(locale -> System.out.printf(
				"Locale code : %s  \t -> %s%n", locale, locale.getDisplayName()));
		
		coutln("0= " + locales[0]);
		cout("1= " + locales[1]);
		
	}//</main>

}//</class>
