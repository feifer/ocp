package c13_Localization;

import static util.X.___separator;
import static util.X.cout;
import static util.X.cout2;
import static util.X.coutln;

import java.util.Arrays;
import java.util.Locale;

public class AvailableLocales2 {

	public AvailableLocales2() {

	}

	public static void main(String[] args) {

		___separator();
		cout2("Experimenting with Locales...");

		coutln("Filter English locales from available locales:");
		cout("");

		Arrays.stream(Locale.getAvailableLocales())
				.filter(locale -> locale.getLanguage().equals("en"))
				.forEach(locale -> System.out.printf("Locale code: %s -> %s %n",
						locale, locale.getDisplayName()));
	}

}
