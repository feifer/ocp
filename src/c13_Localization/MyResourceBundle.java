package c13_Localization;

import java.util.Locale;
import java.util.ResourceBundle;

import static util.X.___separator;
import static util.X.cout2;
import static util.X.cout;

public class MyResourceBundle {

	public MyResourceBundle() {

	}

	public static void main(String[] args) {

		Locale currentLocale = Locale.getDefault();
		ResourceBundle resBundle = ResourceBundle.getBundle("ResourceBundle",
				currentLocale);

		cout("Greeting: " + resBundle.getString("Greeting"));
	}

}
