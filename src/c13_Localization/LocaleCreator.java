package c13_Localization;

import static util.X.___separator;
import static util.X.cout2;
import static util.X.cout;

import java.util.Locale;

public class LocaleCreator {

	public LocaleCreator() {

	}

	public static void main(String[] args) {

		___separator();
		cout2("create a Locale Variant 1");
		Locale locale1 = new Locale("it", "", "");
		cout("locale1= " + locale1.getLanguage());

		___separator();
		cout2("create a Locale Variant 2");
		Locale locale2 = Locale.forLanguageTag("it");
		cout("locale2= " + locale2.getLanguage());

		___separator();
		cout2("create a Locale Variant 3");
		Locale locale3 = new Locale.Builder().setLanguageTag("it").build();
		cout("locale3= " + locale3.getLanguage());

		___separator();
		cout2("create a Locale Variant 4");
		Locale locale4 = Locale.ITALIAN;
		cout("locale4= " + locale4.getLanguage());

		___separator();
		cout2("Experimenting with getDisplayCountry(Locale)");
		
		cout("Locale.ENGLISH -> \t"
				+ Locale.GERMANY.getDisplayCountry(Locale.ENGLISH));

		cout("Locale.GERMANY -> \t"
				+ Locale.GERMANY.getDisplayCountry(Locale.GERMANY));

		cout("Locale.CHINA -> \t" + Locale.GERMANY.getDisplayCountry(Locale.CHINA));

		cout("Locale.CANADA_FRENCH -> \t"
				+ Locale.GERMANY.getDisplayCountry(Locale.CANADA_FRENCH));

		cout("Locale.FRANCE -> \t"
				+ Locale.GERMANY.getDisplayCountry(Locale.FRANCE));

		cout("Locale.ITALIAN -> \t"
				+ Locale.GERMANY.getDisplayCountry(Locale.ITALIAN));

		cout("Locale.JAPAN -> \t" + Locale.GERMANY.getDisplayCountry(Locale.JAPAN));

		cout("Locale.SIMPLIFIED_CHINESE -> \t"
				+ Locale.GERMANY.getDisplayCountry(Locale.SIMPLIFIED_CHINESE));

		cout("Locale.forLanguageTag(\"pl\") -> \t" + Locale.GERMANY
				.getDisplayCountry(Locale.forLanguageTag("pl")));
		cout("Locale.forLanguageTag(\"sl\") -> \t" + Locale.GERMANY
				.getDisplayCountry(Locale.forLanguageTag("sl")));
		cout("Locale.forLanguageTag(\"be\") -> \t" + Locale.GERMANY
				.getDisplayCountry(Locale.forLanguageTag("be")));

	}

}
